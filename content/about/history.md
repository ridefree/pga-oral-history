---
title: "Brief History of the PGA"
date: 2020-05-20T21:25:41-04:00
draft: false
---


### A Brief History of the PGA

Peoples’ Global Action became the initial linking network that inspired the decentralized global days of action
against neoliberalism between 1998 and 2001. Hundreds of thousands of people took to the streets in over 60
countries.
The first
[Global Action Days](https://www.nadir.org/nadir/initiativ/agp/en/pgainfos/bulletin2/bulletin2b.html),
was during the 2nd WTO ministerial conference in Geneva in May 1998.Subsequent Global Action Days have included
those against the G8 ([June 18/1999](https://www.nadir.org/nadir/initiativ/agp/free/global/j18.htm)),
the 3rd WTO summit in Seattle ([November 30/1999](https://www.nadir.org/nadir/initiativ/agp/free/seattle/n30/index.htm)),
 forcing the collapse
of the 1999 Seattle talks of the World Trade Organization trying to promote
neoliberal economics. More Global Action Days challenged the World Bank meeting
in Prague ([September 26/2000](https://www.nadir.org/nadir/initiativ/agp/s26/index.htm)),
and the 4th WTO summit in Qatar ([November 2001](https://www.nadir.org/nadir/initiativ/agp/free/qatar/index.html">)).

Groups involved in PGA have also organized Caravans, regional conferences, workshops and
other events in many regions of the world. Since Geneva, PGA conferences have
been held in <b><a
href="https://www.nadir.org/nadir/initiativ/agp/bangalore/index.htm">Bangalore</a></b>,
India (1999), and <b><a
href="https://www.nadir.org/nadir/initiativ/agp/cocha/index.htm">Cochabamba</a></b>,
Bolivia (2001).

PGA built deep relations of
solidarity amongst activists in very different locations and movements through
its activist tours, global and regional conferences and networks, stimulating a
global circulation of forms of struggle, values and perspectives. Without many
material resources, what linked these movements were a core set of hallmarks,
which endure in many organisations, networks and coalitions formed during its
heyday. These included a rejection of the previously dominant strategy of
reforming global institutions; outright refusal of capitalism, patriarchy and
racism; an orientation towards confrontation rather than lobbying; a call for
direct action and civil disobedience; and an organisational philosophy of
decentralisation and autonomy. PGA marked the then highpoint of a practice of
radical transnationality which was nonetheless fiercely assertive of the
importance of local situations and struggles and hostile to attempts to subsume
such local specificities under supposedly universal assumptions.

Eighteen years later, new generations
of struggle have come on the scene, but the impact of PGA and the movements and
networks it stimulated can still be felt. Many of the activists of this earlier
period have stories, and lessons about what worked (or didn't), about
solidarity, about relationships built across diverse contexts – tales of
failure and success. A loose network of movement activists/scholars are hoping
to facilitate a research project to ensure that these stories and insights
aren’t lost.<span style='mso-spacerun:yes'>  </span>This is not just about the
past, but is a collective reflection: how can those experiences help the
movements of today – and tomorrow. For more information or to get involved
please contact <a href="mailto:pgaoralhistory@tao.ca">pgaoralhistory@tao.ca</a>



<a href="https://www.nadir.org/nadir/initiativ/agp/en/index.html">pga en</a> | <a
href="https://www.nadir.org/nadir/initiativ/agp/index.html">www.agp.org</a>
