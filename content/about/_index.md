+++
title = "About"
date = 2020-05-20T21:21:01-04:00
weight = 1
+++

February 1998, movements from all continents met in Geneva to launch a worldwide coordination of
resistances to the global market and so-called &quot;Free&quot; Trade lead by
big corporations and the World Trade Organization (WTO).

Inspired by the Zapatistas,
[Peoples’ Global Action](https://www.nadir.org/nadir/initiativ/agp/)
was launched linking movements across the globe. Rejecting northern-dominated
models of international solidarity, this platform was defined by the [PGA hallmarks](https://www.nadir.org/nadir/initiativ/agp/en/#hallmarks),
[manifesto](https://www.nadir.org/nadir/initiativ/agp/en/pgainfos/manifest.htm)
and [principles](https://www.nadir.org/nadir/initiativ/agp/cocha/principles.htm).

It became the initial linking network that inspired the decentralized global days of action against
neoliberalism between 1998 and 2001. Hundreds of thousands of people took to the streets in over 60
countries. Groups involved in PGA have also organized Caravans, regional conferences, two global conferences, workshops and other events in many regions of the world.

TODAY: Young people and social movement organizers want to know more about these inspiring
movements. For this reason a PGA oral history project is being set up. Many of
the activists of this earlier period have stories and lessons about solidarity,
about what worked (or didn't), about relationships built across diverse
contexts – tales of failure and success. A loose network of movement
activists/scholars are hoping to facilitate an oral history project to ensure
that these stories and insights aren’t lost.

For more information and to get involved please contact [pgaoralhistory](mailto:pgaoralhistory@tao.ca)
coordinators: [North America](mailto:lesley.j.wood@gmail.com), [Europe](mailto:Laurence.Cox@nuim.ie), [South America](mailto:pgaoralhistory@tao.ca).

Click [here](https://vimeo.com/173037314) for a 10 minute documentary of the PGA.

And [more in writing](https://www.degrowth.info/wp-content/uploads/2016/12/DIM_PGA.pdf).

Courtesy [degrowth.info](https://www.degrowth.info/en/dim/degrowth-in-movements/peoples-global-action/)

pgaoralhistory.net

[pga en](https://www.nadir.org/nadir/initiativ/agp/en/index.html) | [www.agp.org](https://www.nadir.org/nadir/initiativ/agp/index.html)
